import React from 'react';

import {Provider as ReduxProvider} from 'react-redux';

import store from './src/store';
import Routes from './src/routes';

const App: React.FC = () => {
  return (
    <ReduxProvider store={store}>
      <Routes />
    </ReduxProvider>
  );
};

export default App;
