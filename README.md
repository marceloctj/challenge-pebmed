<p align="center">

  <h3 align="center">PEBMED Challenge</h3>

  <p align="center">
    Aplicativo para listar os conteúdos e seus detalhes, com funcionamento offline first.
  </p>
  <br />
</p>

## Tabela de Conteúdos

* [Começando](#começando)
	* [Pré-Requisitos](#pré-requisitos)
	* [Instalação](#instalação)
* [Executando o Projeto](#executando-o-projeto)
* [Testes](#testes)

## Começando

### Pré-Requisitos

Esse projeto necessita que esteja previamente instalado:

* [NodeJS - Versão 12 ou Superior](https://nodejs.org/en/)
* [Yarn](https://yarnpkg.com/en/docs/install)
* [React Native](https://reactnative.dev/docs/environment-setup)
* [CocoaPods](https://cocoapods.org/)

###### Obs: Para executar o projeto, todo o ambiente deverá estar configurado previamente para Android e/ou iOS.

### Instalação

1. Clone o repositório
````sh
git clone https://gitlab.com/marceloctj/challenge-pebmed.git
````

2. Instale as dependências do projeto
````sh
yarn
````

3. Copie o arquivo env.json.example para env.json
````sh
cp env.json.example env.json
````

4. Instale os Pods do projeto iOS (Somente iOS):
````sh
npx pod-install;
````
Se não possuir a extensão npx do (NPM) configurada, execute:
````sh
cd ios;
pod install;
````

## Executando o Projeto

Após realizar todos os passos de instalação, execute o comando:

#### Para iOS
````sh
yarn ios
````

#### Para Android
````sh
yarn android
````

##### Obs.:
Ao executar os comandos "yarn android" ou "yarn ios", é executado o script "npm run cache:generate" o arquivo do script está em /scripts/generateCacheDB.ts.
Esse script cria o arquivo de banco de dados para o cache dos conteúdos, nas pastas de cada plataformas(Android e iOS).
