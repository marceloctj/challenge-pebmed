/**
 * @format
 */
import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import App from './App.tsx';
import {name as appName} from './app.json';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import './src/cache/index';

Icon.loadFont();

AppRegistry.registerComponent(appName, () => App);
