import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  flex: 1;
`;

export const ScrollView = styled.ScrollView.attrs({
  contentContainerStyle: {
    marginHorizontal: 20,
    paddingVertical: 30,
  },
})``;

export const Text = styled.Text`
  margin-bottom: 24px;
  color: #333;
  font-size: 14px;
`;

export const FavButtonContainer = styled.View`
  position: absolute;
  top: -35px;
  right: 20px;
  z-index: 5;
`;
