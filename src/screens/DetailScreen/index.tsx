import React from 'react';
import {RouteProp, useRoute} from '@react-navigation/native';
import {StatusBar} from 'react-native';

import Title from '../../components/Title';

import {Container, ScrollView, Text, FavButtonContainer} from './styles';

import CustomHeader from '../../components/layout/CustomHeader';
import FabButton from '../../components/FabButton';
import {useFavoriteContent} from '../../utils/hooks/contents';

type ParamList = {
  item: any;
};

const DetailScreen: React.FC = () => {
  const route = useRoute<RouteProp<ParamList, 'item'>>();

  const item = route.params?.item;

  const authors = item.content.authors
    .map((author: any) => author.name)
    .join('\n');

  const {isFavorite, toggleIsFavorite} = useFavoriteContent(item);

  const handleOnPressFavButton = () => {
    toggleIsFavorite();
  };

  const favIcon = isFavorite
    ? {iconName: 'heart', iconColor: '#f00', animated: true}
    : {iconName: 'heart-outline', iconColor: '#fff', animated: false};

  return (
    <>
      <StatusBar barStyle="light-content" />
      <CustomHeader image={item.content.urlImage} title={item.content.name} />
      <Container>
        <FavButtonContainer>
          <FabButton
            {...favIcon}
            color="#1a1947"
            onPress={handleOnPressFavButton}
            animationType="pulse"
          />
        </FavButtonContainer>
        <ScrollView>
          <Title>Descrição</Title>
          <Text>{item.content.description}</Text>

          <Title>Autores</Title>
          <Text>{authors}</Text>
        </ScrollView>
      </Container>
    </>
  );
};

export default DetailScreen;
