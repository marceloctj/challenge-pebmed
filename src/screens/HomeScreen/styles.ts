import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  flex: 1;
`;

export const ScrollView = styled.ScrollView.attrs({
  contentContainerStyle: {
    marginHorizontal: 20,
    paddingVertical: 40,
  }
})``;

export const Row = styled.View`
  flex-direction: row;
  margin-bottom: 10px;
`;
export const Separator = styled.View`
  margin-left: 20px;
`;

export const CardFake = styled.View`
  flex: 1;
`;
