import React, {Fragment, useEffect} from 'react';
import {View} from 'react-native';

import _ from 'lodash';
import {useDispatch, useSelector} from 'react-redux';

import {useNetInfo} from '@react-native-community/netinfo';

import Title from '../../components/Title';
import {Container, Row, ScrollView, Separator, CardFake} from './styles';

import Card from './components/Card';

import {loadContents} from '../../store/ducks/contents';
import {RootState} from '../../store/ducks';
import Loading from '../../components/Loading';

const HomeScreen: React.FC = () => {
  const dispatch = useDispatch();

  const {data, loading}: any = useSelector<RootState>(
    (state) => state.contents,
  );

  const netInfo = useNetInfo();

  useEffect(() => {
    dispatch(loadContents());
  }, [dispatch]);

  useEffect(() => {
    if (netInfo.isConnected) {
      dispatch(loadContents());
    }
  }, [dispatch, netInfo.isConnected]);

  if (loading) {
    return <Loading />;
  }

  return (
    <Container>
      <ScrollView>
        {data?.map((section: any) => (
          <View key={`${section.category.id}`} style={{marginBottom: 30}}>
            <Title>{section.category.name}</Title>
            {_.chunk(section.data, 2).map((items, groupIndex) => (
              <Row key={groupIndex}>
                {items.map((item: any, index) => (
                  <Fragment key={item.content.id}>
                    <Card item={item} />
                    {index === 0 && <Separator />}
                    {items.length === 1 && <CardFake />}
                  </Fragment>
                ))}
              </Row>
            ))}
          </View>
        ))}
      </ScrollView>
    </Container>
  );
};

export default HomeScreen;
