import LinearGradient from 'react-native-linear-gradient';
import styled from 'styled-components/native';
import CustomImage from '../../../../components/CustomImage';

export const Container = styled.Pressable`
  flex: 1;
`;

export const ImageContainer = styled.View`
  position: relative;
`;

export const Image = styled(CustomImage)`
  height: 100px;
  width: 100%;
  border-radius: 10px;
`;

export const ImageOverlay = styled(LinearGradient).attrs({
  colors: ['#0000', '#000'],
  locations: [0.4, 1],
})`
  height: 100px;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 2;
  border-radius: 10px;
`;

export const Text = styled.Text`
  margin-vertical: 20px;
  color: #333;
  font-size: 14px;
  padding-left: 10px;
`;

export const FavButton = styled.Pressable`
  width: 50px;
  height: 50px;
  justify-content: center;
  align-items: center;
  position: absolute;
  bottom: 0;
  right: 0;
  z-index: 3;
`;
