import React, {useCallback, memo} from 'react';
import {useNavigation} from '@react-navigation/native';

import {
  Container,
  FavButton,
  ImageContainer,
  ImageOverlay,
  Image,
  Text,
} from './styles';

import {useFavoriteContent} from '../../../../utils/hooks/contents';
import AnimatedIcon from '../../../../components/AnimatedIcon';

type Props = {
  item: any;
};

const Card: React.FC<Props> = ({item}) => {
  const {isFavorite, toggleIsFavorite} = useFavoriteContent(item);

  const navigation = useNavigation();

  const favIcon = isFavorite
    ? {name: 'heart', color: '#f00', animated: true}
    : {name: 'heart-outline', color: '#fff', animated: false};

  const handleOnPressFavButton = () => {
    toggleIsFavorite();
  };

  const handleOnPressNavigateToDetail = useCallback(() => {
    navigation.navigate('detail', {item});
  }, [item, navigation]);

  return (
    <Container onPress={handleOnPressNavigateToDetail}>
      <ImageContainer>
        <ImageOverlay />
        <Image source={{uri: item.content.urlImage}} />
        <FavButton onPress={handleOnPressFavButton}>
          <AnimatedIcon {...favIcon} animationType="pulse" size={20} />
        </FavButton>
      </ImageContainer>
      <Text>{item.content.name}</Text>
    </Container>
  );
};

export default memo(Card);
