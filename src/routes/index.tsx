import React from 'react';
import {View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';

import HomeScreen from '../screens/HomeScreen';
import DetailScreen from '../screens/DetailScreen';
import Header from '../components/layout/Header';
import OpenDrawerButton from '../components/OpenDrawerButton';

const Stack = createStackNavigator();

const Drawer = createDrawerNavigator();

const Routes: React.FC = () => {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="main">
        <Drawer.Screen
          name="main"
          options={{drawerLabel: 'Home'}}
          component={MainRoutes}
        />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

const MainRoutes = () => (
  <Stack.Navigator initialRouteName="home">
    <Stack.Screen
      name="home"
      component={HomeScreen}
      options={{
        headerTitle: Header,
        headerLeft: () => <OpenDrawerButton />,
        headerRight: () => <View />,
      }}
    />
    <Stack.Screen
      name="detail"
      component={DetailScreen}
      options={{headerShown: false}}
    />
  </Stack.Navigator>
);

export default Routes;
