import {ReduxActionType} from './';

export const Types = {
  ASYNC_LOAD_CONTENTS: '@CONTENTS/ASYNC_LOAD_CONTENTS',
  CONTENS_LOADED: '@CONTENTS/CONTENTS_LOADED',
  LOADING_SETTED: '@CONTENTS/LOADING_SETTED',
  ASYNC_FAVORITE_CONTENT: '@CONTENTS/ASYNC_FAVORITE_CONTENT',
  FAVORITE_SETTED: '@CONTENTS/FAVORITE_SETTED',
};

type STATE = {
  data?: Array<any>;
  loading: boolean;
  favorites: number[];
};

const INITIAL_STATE: STATE = {
  data: undefined,
  loading: false,
  favorites: [],
};

export default (state = INITIAL_STATE, {type, payload}: ReduxActionType) => {
  switch (type) {
    case Types.CONTENS_LOADED:
      return {...state, data: payload};
    case Types.LOADING_SETTED:
      return {...state, loading: payload};
    case Types.FAVORITE_SETTED:
      return {...state, favorites: payload};
    default:
      return state;
  }
};

export const setLoading = (loading: boolean) => ({
  type: Types.LOADING_SETTED,
  payload: loading,
});

export const setFavorites = (favorites: number[]) => ({
  type: Types.FAVORITE_SETTED,
  payload: favorites,
});

export const favorite = (id: number) => ({
  type: Types.ASYNC_FAVORITE_CONTENT,
  payload: {id},
});

export const loadContents = () => ({
  type: Types.ASYNC_LOAD_CONTENTS,
});

export const setContents = (contents: any[]) => ({
  type: Types.CONTENS_LOADED,
  payload: contents,
});
