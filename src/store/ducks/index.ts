import {combineReducers} from 'redux';

import contentsReducer from './contents';

const rootReducer = combineReducers({
  contents: contentsReducer,
});

export default rootReducer;

export type ReduxActionType = {
  type: string;
  payload: any;
};

export type RootState = ReturnType<typeof rootReducer>;
