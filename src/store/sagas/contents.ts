import {
  all,
  call,
  put,
  takeLatest,
  takeEvery,
  select,
  fork,
} from 'redux-saga/effects';

import NetInfo, {NetInfoState} from '@react-native-community/netinfo';

import {AnyAction} from 'redux';

import * as service from '../../services/contents';
import {Types, setContents, setLoading, setFavorites} from '../ducks/contents';

import {
  getContentsWithCacheFormat,
  getUniqueCategoriesByContents,
  groupContentsByCategory,
} from './../../utils/formaters/contents';

import contentRepository from '../../cache/content/repository';
import categoryRepository from '../../cache/category/repository';

import {Content} from './../../cache/content/types';
import {Category} from './../../cache/category/types';

function* getOnlineOrOfflineContents() {
  const netinfo: NetInfoState = yield call(NetInfo.fetch);

  if (netinfo.isConnected) {
    try {
      return {online: true, contents: yield call(service.getContents)};
    } catch {}
  }

  const contents: Content[] = yield call<() => Promise<Content[]>>(
    contentRepository.all,
  );
  const categories: Category[] = yield call<() => Promise<Category[]>>(
    categoryRepository.all,
  );

  const formatedContents = contents.map((content) => ({
    content: {...content, authors: JSON.parse(`${content.authors}`)},
    category: categories.find(
      (category) => category.id === content.category_id,
    ),
  }));

  return {online: false, contents: formatedContents};
}

function* syncContentsCacheDB(data: any) {
  try {
    yield call(contentRepository.truncate);
    yield call(categoryRepository.truncate);

    const categories = getUniqueCategoriesByContents(data);
    yield call(categoryRepository.createBulk, categories);

    const contents = getContentsWithCacheFormat(data);
    yield call(contentRepository.createBulk, contents);
  } catch (err) {}
}

function* loadContents() {
  yield put(setLoading(true));
  const {online, contents} = yield call(getOnlineOrOfflineContents);

  try {
    if (online) {
      yield fork(syncContentsCacheDB, contents);
    }
    const contentsGroupedByCategories = groupContentsByCategory(contents);
    yield put(setContents(contentsGroupedByCategories));
  } catch (err) {
  } finally {
    yield put(setLoading(false));
  }
}

function* favoriteContent(action: AnyAction) {
  const favorites = yield select((state) => state.contents.favorites);
  let newFavorites: number[] = [];

  if (favorites.includes(action?.payload.id)) {
    newFavorites = favorites.filter(
      (favorite: number) => favorite !== action?.payload.id,
    );
  } else {
    newFavorites = [...favorites, action?.payload.id];
  }

  yield put(setFavorites(newFavorites));
}

export default function* contentsSaga() {
  yield all([
    takeLatest(Types.ASYNC_LOAD_CONTENTS, loadContents),
    takeEvery(Types.ASYNC_FAVORITE_CONTENT, favoriteContent),
  ]);
}
