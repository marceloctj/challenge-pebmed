import {all} from 'redux-saga/effects';
import contentsSaga from './contents';

export default function* rootSagas() {
  yield all([contentsSaga()]);
}
