import {Transaction} from 'react-native-sqlite-storage';
import cache from '..';
import {Category} from './types';

class CategoryRepository {
  private table = 'categories';

  public all = async (): Promise<Category[]> => {
    try {
      const [results] = await cache
        .getConnection()
        .executeSql(`SELECT * FROM ${this.table}`);

      return results.rows.raw();
    } catch {
      return [];
    }
  };

  public create = async (category: Category, tx?: Transaction) => {
    const query = `INSERT INTO ${this.table}(id, name) values (?,?)`;
    return (tx || cache.getConnection()).executeSql(query, [
      category.id,
      category.name,
    ]);
  };

  public createBulk = async (categories: Category[]): Promise<void> => {
    cache
      .getConnection()
      .transaction((tx: Transaction) =>
        Promise.all(categories.map((category) => this.create(category, tx))),
      );
  };

  public truncate = async (): Promise<void> => {
    await cache.truncate(this.table);
  };
}

export default new CategoryRepository();
