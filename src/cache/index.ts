import SQLite, {SQLiteDatabase, Transaction} from 'react-native-sqlite-storage';

SQLite.enablePromise(true);
class CacheDatabase {
  private connection!: SQLiteDatabase;

  constructor() {
    this.makeConnection();
  }

  private async makeConnection() {
    this.connection = await SQLite.openDatabase({
      name: 'cache.db',
      createFromLocation: 1,
      location: 'default',
    });
  }

  public getConnection(): SQLiteDatabase {
    return this.connection;
  }

  public async truncate(table: string) {
    await this.getConnection().transaction((tx: Transaction) => {
      return tx.executeSql(`DELETE FROM ${table}`);
    });
  }
}

export default new CacheDatabase();
