export type Content = {
  id: number;
  name: string;
  urlImage?: string;
  description?: string;
  authors: Object | string;
  category_id?: number;
};
