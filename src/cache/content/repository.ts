import {Transaction} from 'react-native-sqlite-storage';
import cache from '..';
import {Content} from './types';

class ContentRepository {
  private table = 'contents';

  public all = async (): Promise<Content[]> => {
    try {
      const [results] = await cache
        .getConnection()
        .executeSql(`SELECT * FROM ${this.table}`);

      return results.rows.raw();
    } catch {
      return [];
    }
  };

  public create = async (content: Content, tx?: Transaction) => {
    const query = `INSERT INTO ${this.table}(id, name, urlImage, description, authors, category_id) values (?,?,?,?,?,?)`;
    return (tx || cache.getConnection()).executeSql(query, [
      content.id,
      content.name,
      content.urlImage,
      content.description,
      content.authors,
      content.category_id,
    ]);
  };

  public createBulk = async (contents: Content[]): Promise<void> => {
    cache
      .getConnection()
      .transaction((tx: Transaction) =>
        Promise.all(contents.map((content) => this.create(content, tx))),
      );
  };

  public truncate = async (): Promise<void> => {
    await cache.truncate(this.table);
  };
}

export default new ContentRepository();
