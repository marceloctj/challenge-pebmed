import client from '.';

export const getContents = async () => {
  const {data} = await client.get('contents');
  return data;
};
