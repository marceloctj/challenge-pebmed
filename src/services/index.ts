import axios from 'axios';

import env from '../../env.json';

const client = axios.create({
  baseURL: env.api.baseURL,
  timeout: env.api.timeout,
});

export default client;
