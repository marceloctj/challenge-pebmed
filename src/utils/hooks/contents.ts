/* eslint-disable react-hooks/rules-of-hooks */
import {useCallback} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {favorite} from '../../store/ducks/contents';
import {RootState} from '../../store/ducks/index';

export const setFavoriteContent = (item: any) => {
  const dispatch = useDispatch();

  return useCallback(() => {
    dispatch(favorite(parseInt(item.content.id, 10)));
  }, [dispatch, item.content.id]);
};

export const useFavoriteContent = (item: any) => {
  const contents: any = useSelector<RootState>((state) => state.contents);

  const isFavorite =
    contents.favorites.find((favoriteId: number) => {
      return favoriteId === item.content.id;
    }) !== undefined;

  const toggleIsFavorite = setFavoriteContent(item);

  return {toggleIsFavorite, isFavorite};
};
