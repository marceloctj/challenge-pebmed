export const groupContentsByCategory = (contents: any[]) => {
  return contents.reduce(function (sections: any, item) {
    let currentSection: any = sections.find(
      (section: any) => section.category.id === item.category.id,
    );

    if (!currentSection) {
      currentSection = {category: item.category, data: []};
      sections.push(currentSection);
    }

    currentSection.data.push(item);

    return sections;
  }, []);
};

export const getUniqueCategoriesByContents = (contents: any[]) => {
  return contents.reduce((acumulador: any[], current) => {
    return !acumulador.find((item) => item.id === current.category.id)
      ? [...acumulador, current.category]
      : acumulador;
  }, []);
};

export const getContentsWithCacheFormat = (contents: any[]) => {
  return contents.map(({content, category}) => ({
    id: content.id,
    name: content.name,
    urlImage: content.urlImage,
    description: content.description,
    authors: JSON.stringify(content.authors),
    category_id: category.id,
  }));
};
