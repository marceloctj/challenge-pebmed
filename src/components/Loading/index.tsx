import React from 'react';
import {ActivityIndicator} from 'react-native';

import styled from 'styled-components/native';

export const ContainerDefault = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

export const ContainerAbsolute = styled(ContainerDefault)`
  position: absolute;
  z-index: 99;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
`;

type Props = {
  absolute?: boolean;
  color?: string;
};

const Loading: React.FC<Props> = ({absolute = false, color = '#1a1947'}) => {
  const Container = absolute ? ContainerAbsolute : ContainerDefault;
  return (
    <Container>
      <ActivityIndicator size="small" color={color} />
    </Container>
  );
};

export default Loading;
