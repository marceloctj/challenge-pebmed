import React, {memo, useCallback, useRef} from 'react';

import {Animated} from 'react-native';

import AnimatedIcon from '../AnimatedIcon';

import {Container} from './styles';

type Props = {
  iconName: string;
  iconSize?: number;
  iconColor: string;
  color: string;
  onPress?: any;
  animationType?: string;
  animated?: boolean;
};

const FabButton: React.FC<Props> = ({
  iconName,
  iconSize = 24,
  iconColor,
  color,
  onPress,
  animationType,
  animated = false,
}) => {
  return (
    <Container bgColor={color} onPress={onPress}>
      <AnimatedIcon
        animationType={animationType}
        animated={animated}
        name={iconName}
        size={iconSize}
        color={iconColor}
      />
    </Container>
  );
};

export default memo(FabButton);
