import styled from 'styled-components/native';

type ContainerProps = {
  bgColor?: string;
};

export const Container = styled.Pressable<ContainerProps>`
  background-color: ${({bgColor}) => bgColor || '#ccc'};
  border-radius: 60px;
  width: 60px;
  height: 60px;
  justify-content: center;
  align-items: center;
`;
