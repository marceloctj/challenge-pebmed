import React from 'react';
import {useWindowDimensions} from 'react-native';

import BackButton from '../../BackButton';

import {Container, Overlay, Content, Text} from './styles';

type Props = {
  image: string;
  title: string;
  children?: React.ReactChild;
};

const CustomHeader: React.FC<Props> = ({image, title, children}) => {
  const dimensions = useWindowDimensions();
  const height = dimensions.height * 0.3;
  return (
    <Container source={{uri: image}} height={height > 240 ? 240 : height}>
      <Overlay>
        <Content>
          <BackButton />
          <Text>{title}</Text>
        </Content>
      </Overlay>
      {children}
    </Container>
  );
};

export default CustomHeader;
