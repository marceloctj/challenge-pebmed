import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';
import {Platform} from 'react-native';

export const Container = styled.ImageBackground`
  height: ${({height}) => height}px;
  box-shadow: 1px 2px 2px #00000063;
`;

export const Overlay = styled(LinearGradient).attrs({
  colors: ['#000', '#0000'],
  locations: [0, 1],
})`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 2;
`;

export const Content = styled.SafeAreaView`
  flex-direction: row;
  align-items: center;
  margin-top: ${Platform.select({ios: 40, android: 20})}px;
`;

export const Text = styled.Text`
  color: #fff;
  font-size: 18px;
  margin-left: 20px;
`;
