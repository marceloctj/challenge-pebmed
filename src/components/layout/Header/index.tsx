import React from 'react';

import {Container, Text, TextBold} from './styles';

const Header: React.FC = () => {
  return (
    <Container>
      <Text>
        White<TextBold>Book</TextBold>
      </Text>
    </Container>
  );
};

export default Header;
