import styled from 'styled-components/native';

export const Container = styled.SafeAreaView`
  align-items: center;
  justify-content: center;
`;

export const Text = styled.Text`
  color: #000;
  font-size: 16px;
  text-transform: uppercase;
  font-weight: 200;
`;

export const TextBold = styled.Text`
  font-weight: bold;
`;
