import {Animated} from 'react-native';

const stepAnimation = (value: number) => ({
  toValue: value,
  duration: 500,
  useNativeDriver: false,
});

const animations: any = {
  pulse: ({
    fontSizeValue,
    fontSize,
  }: {
    fontSizeValue: any;
    fontSize: number;
  }) => {
    Animated.sequence([
      Animated.timing(fontSizeValue, stepAnimation(fontSize + 6)),
      Animated.timing(fontSizeValue, stepAnimation(fontSize)),
      Animated.timing(fontSizeValue, stepAnimation(fontSize + 6)),
      Animated.timing(fontSizeValue, stepAnimation(fontSize)),
    ]).start();
  },
};

export default (animationType: string) => {
  return animations[animationType] || (() => {});
};
