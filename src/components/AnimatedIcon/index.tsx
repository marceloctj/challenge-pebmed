/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useRef} from 'react';
import {Animated} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import animations from './animations';

const AnimatedIconComponent = Animated.createAnimatedComponent(Icon);

type Props = {
  name: string;
  size?: number;
  color: string;
  animationType?: string;
  animated?: boolean;
};

const AnimatedIcon: React.FC<Props> = ({
  animationType,
  animated,
  size = 24,
  name,
  color,
}) => {
  const fontSizeValue = useRef(new Animated.Value(size)).current;

  useEffect(() => {
    if (animated && animationType) {
      animations(animationType)({fontSizeValue: fontSizeValue, fontSize: size});
    }
  }, [animated, animationType]);

  return (
    <AnimatedIconComponent
      style={{fontSize: fontSizeValue}}
      name={name}
      color={color}
    />
  );
};

export default AnimatedIcon;
