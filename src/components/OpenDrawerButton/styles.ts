import styled from 'styled-components/native';

export const Container = styled.Pressable`
  padding: 6px 20px;
`;
