import React from 'react';
import {StackHeaderLeftButtonProps} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Container} from './styles';

import {DrawerActions, useNavigation} from '@react-navigation/native';

const OpenDrawerButton: React.FC<StackHeaderLeftButtonProps> = () => {
  const navigation = useNavigation();

  return (
    <Container
      onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}>
      <Icon name="menu" size={20} />
    </Container>
  );
};

export default OpenDrawerButton;
