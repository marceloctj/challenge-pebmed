import React, {ReactChild} from 'react';

import {Container, Text} from './styles';

type Props = {
  text?: string;
  children?: ReactChild;
};

const Title: React.FC<Props> = ({text, children}) => {
  return (
    <Container>
      <Text>{children || text}</Text>
    </Container>
  );
};

export default Title;
