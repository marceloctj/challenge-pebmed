import styled from 'styled-components/native';

export const Container = styled.View``;

export const Text = styled.Text`
  color: #000;
  font-size: 16px;
  font-weight: 600;
  margin-bottom: 20px;
  text-transform: uppercase;
`;
