import React from 'react';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Container} from './styles';

const BackButton: React.FC = () => {
  const navigation = useNavigation();
  return (
    <Container onPress={() => navigation.goBack()}>
      <Icon name="arrow-left" size={24} color="#fff" />
    </Container>
  );
};

export default BackButton;
