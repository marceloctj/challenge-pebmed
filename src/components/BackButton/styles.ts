import styled from 'styled-components/native';

export const Container = styled.Pressable`
  width: 50px;
  height: 50px;
  justify-content: center;
  align-items: center;
  margin-left: 12px;
`;
