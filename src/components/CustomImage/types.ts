import {ImageProps, ImageSourcePropType, StyleProp} from 'react-native';

export type CustomImageProps = {
  style?: StyleProp<ImageProps>;
  source: ImageSourcePropType;
};

export enum ImageErrorsEnum {
  default = 'default',
  offline = 'offline',
}

export type ImageErrorProps = {
  style?: StyleProp<ImageProps>;
  error: ImageErrorsEnum;
};
