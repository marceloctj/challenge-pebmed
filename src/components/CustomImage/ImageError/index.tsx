import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styled from 'styled-components/native';
import {ImageErrorProps, ImageErrorsEnum} from '../types';

const errorIcon = {
  [ImageErrorsEnum.default]: 'alert-circle',
  [ImageErrorsEnum.offline]: 'network-strength-off',
};

const Container = styled.View`
  background-color: #ccc;
  justify-content: center;
  align-items: center;
`;

const ImageError: React.FC<ImageErrorProps> = ({error, style}) => {
  return (
    <Container style={style}>
      <Icon name={errorIcon[error]} color="#999" size={24} />
    </Container>
  );
};

export default ImageError;
