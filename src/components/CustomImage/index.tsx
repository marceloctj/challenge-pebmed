import React, {useState} from 'react';

import {Image as ImageCore} from 'react-native';

import Loading from '../Loading';

import {CustomImageProps, ImageErrorsEnum} from './types';
import ImageError from './ImageError';

const CustomImage: React.FC<CustomImageProps> = ({style, source}) => {
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<ImageErrorsEnum | null>(null);

  return (
    <>
      {loading && !error && <Loading absolute color="#666" />}
      {error === null ? (
        <ImageCore
          style={style}
          source={source}
          onLoadEnd={() => setLoading(false)}
          onError={() => setError(ImageErrorsEnum.default)}
        />
      ) : (
        <ImageError error={error} style={style} />
      )}
    </>
  );
};

export default React.memo(CustomImage);
