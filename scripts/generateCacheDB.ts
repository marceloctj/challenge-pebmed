import env from '../env.json';

import axios from 'axios';
import fs from 'fs';

import sqlite3 from 'sqlite3';

const cacheFileName = 'cache.db';

const dbFile = `${__dirname}/${cacheFileName}`;

const querysCreate: string[] = [
  `CREATE TABLE "categories" (
    "id"	INTEGER NOT NULL UNIQUE, 
    "name"	VARCHAR(255) NOT NULL,PRIMARY KEY("id")
  )`,
  `CREATE TABLE "contents" (
    "id"	INTEGER NOT NULL UNIQUE,
    "name"	varchar(255) NOT NULL,
    "urlImage"	varchar(255) NOT NULL,
    "description"	TEXT NOT NULL,
    "authors"	JSON NOT NULL,
    "category_id"	INTEGER,
    PRIMARY KEY("id"),
    FOREIGN KEY("category_id") REFERENCES "categories"("id")
  )`,
];

const querys: any = {
  categories: 'INSERT INTO categories values (?, ?)',
  contents: 'INSERT INTO contents values (?, ?, ?, ?, ?, ?)',
};

async function rmIfExists(file: string) {
  try {
    await fs.promises.access(file);
    await fs.promises.rm(file);
  } catch {}
}

async function createOrReplaceDBFile() {
  await rmIfExists(dbFile);
  await fs.promises.writeFile(dbFile, '');
}

async function createFolderIfNotExists(paths: string[]) {
  await Promise.all(
    paths.map((path) =>
      fs.promises.mkdir(path, {recursive: true}).catch(() => {}),
    ),
  );
}

function openDBConnection() {
  const db = new sqlite3.Database(dbFile, (err) => {
    err && console.log('ERR', err);
  });
  return db;
}

function closeDBCollection(db: sqlite3.Database) {
  return new Promise((resolve) => {
    db.close(() => resolve(true));
  });
}

async function getContents() {
  try {
    const {data} = await axios.create(env.api).get('contents');
    return data;
  } catch {
    return [];
  }
}

async function extractCategoriesDBFormat(contents: any[]) {
  return contents
    .reduce((acumulador: any[], current) => {
      return !acumulador.find((item) => item.id === current.category.id)
        ? [...acumulador, current.category]
        : acumulador;
    }, [])
    .map(({id, name}) => [id, name]);
}

async function extractContentsDbFormat(contents: any[]) {
  return contents.map(({content, category}) => [
    content.id,
    content.name,
    content.urlImage,
    content.description,
    JSON.stringify(content.authors),
    category.id,
  ]);
}

function execInsertDB(db: sqlite3.Database, variable: string, data: any[]) {
  const stmt = db.prepare(querys[variable]);

  for (const item of data) {
    stmt.run(...item);
  }

  stmt.finalize();
}

function createTables(db: sqlite3.Database) {
  for (const query of querysCreate) {
    db.run(query);
  }

  return true;
}

(async () => {
  await createOrReplaceDBFile();

  const contents = await getContents();

  const categoriesDbData = await extractCategoriesDBFormat(contents);
  const contentsDbData = await extractContentsDbFormat(contents);

  const db = openDBConnection();

  db.serialize(() => {
    createTables(db);
    execInsertDB(db, 'categories', categoriesDbData);
    execInsertDB(db, 'contents', contentsDbData);
  });

  await closeDBCollection(db);

  const androidDBPath = `${__dirname}/../android/app/src/main/assets/www`;
  const iosDBPath = `${__dirname}/../ios/www`;

  await Promise.all([
    rmIfExists(`${androidDBPath}/${cacheFileName}`),
    rmIfExists(`${iosDBPath}/${cacheFileName}`),
  ]);

  await createFolderIfNotExists([androidDBPath, iosDBPath]);

  await fs.promises.copyFile(dbFile, `${androidDBPath}/${cacheFileName}`);
  await fs.promises.copyFile(dbFile, `${iosDBPath}/${cacheFileName}`);

  await rmIfExists(dbFile);
})();
