describe('Home Screen test suit', () => {
  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should have WHITEBOOK text on the header', async () => {
    const text = await element(by.text('WHITEBOOK'));
    await expect(text).toBeVisible();
  });
});
